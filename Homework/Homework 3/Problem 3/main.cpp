#include <iostream>

using namespace std;

int main()
{
	int boxes;
	double cereal, metric;
	metric = 35273.92;
	char answer;
	answer = 'y';
	while (answer =='y')
	{
		cout << "Enter weight of cereal box (ounces): ";
		cin >> cereal;
		boxes = static_cast<int>(metric / cereal);
		cout << "Cereal boxes: " << boxes << endl;
		cout << "Continue? y/n: ";
		cin >>answer;
	}

	return 0;
}