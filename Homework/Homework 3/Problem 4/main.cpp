#include <iostream>

using namespace std;

int main()
{
	int people, capacity, remainder;
	cout << "Enter room capacity: ";
	cin >> capacity;
	cout << "Enter number of people in the room: ";
	cin >> people;
	if (people <= capacity)
	{
		cout << "It is legal to hold the meeting." << endl;
		remainder = capacity - people;
		cout << "Remaining space: " << remainder << " people." << endl;
	}
	else
	{
		cout << "It is ILLEGAL to hold the meeting due to fire regualtions." << endl;
		remainder = people - capacity;
		cout << "Number of people to be excluded: " << remainder;
	}

	return 0;
}