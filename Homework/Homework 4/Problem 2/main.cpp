#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	char gender;
	int weight, height, age, chocoBars;
	double bmr;
	cout << "Enter weight (lbs.): ";
	cin >> weight;
	cout << "Enter height (inchs.): ";
	cin >> height;
	cout << "Enter age (years): ";
	cin >> age;
	cout << "Enter gender (M/F): ";
	cin >> gender;

	if (gender == 'M')
	{
		bmr = static_cast<double>(66 + (6.3 * weight) + (12.9 * height) - (6.8 * age));
		cout << "BMR: " << bmr << endl;
		chocoBars = static_cast<int>(bmr) / 230;
		cout << "Chococlate bars to be consumed: " << chocoBars;
	}

	else
	{
		bmr = static_cast<double>(655 + (4.3 * weight) + (4.7 * height) - (4.7 * age));
		cout << "BMR: " << bmr << endl;
		chocoBars = static_cast<int>(bmr) / 230;
		cout << "Chococlate bars to be consumed: " << chocoBars;
	}

	return 0;
}