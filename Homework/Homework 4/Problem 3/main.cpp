#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	int n, score, scoreTotal;
	int num;
	int sumScore, sumTotal;
	double average;
	cout << "Enter number of exercises: ";
	cin >> n;
	
	for (int i = 1; i <= n; i++)
	{
		num = n--;
		cout << "Score for exercise " << num << ": ";
		cin >> score;
		cout << "Total possible score " << num << ": ";
		cin >> scoreTotal;
		sumScore = score++;
		sumTotal = scoreTotal++;
	}
	average = static_cast<double>(sumScore / sumTotal);
	cout << "Your total is " << sumScore << " out of " << sumTotal;
	cout << ", or " << average << "." << endl;

	return 0;
}