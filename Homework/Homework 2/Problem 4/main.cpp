#include <iostream>

using namespace std;

int main()
{
    string name1, name2, food, num, adj, color, animal;
    cout << "Enter a name: ";
    cin >> name1;
    cout << "Enter another name: ";
    cin >> name2;
    cout << "Enter a food name: ";
    cin >> food;
    cout << "Enter a number between 100 and 200: ";
    cin >> num;
    cout << "Enter an adjective: ";
    cin >> adj;
    cout << "Enter a color: ";
    cin >> color;
    cout << "Enter an animal name: ";
    cin >> animal;
    cout << endl;
    cout << "Dear " << name1 << "," << endl << endl;
    cout << "I am sorry that I am unable to turn in my homework at this time. "
            "First, I ate a rotten " << food << "," << endl;
    cout << "which made me turn " << color << " and extremely ill. I came down "
            "with a fever of " << num << "." << endl;
    cout << "Next, my " << adj << " pet " << animal << " must have smelled the "
            "remains of the " << food << " on" << endl;
    cout << "my homework because he ate it. I am currently rewriting my homework"
            " and hope you will" << endl;
    cout << "accept it late." << endl;
    cout << "Sincerely," << endl;
    cout << name2;

    return 0;
}