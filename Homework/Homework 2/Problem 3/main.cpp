#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int quiz1_1,quiz1_2, quiz1_3, quiz2_1, quiz2_2, quiz2_3, quiz3_1;
    int quiz3_2, quiz3_3;
    double average1, average2, average3;
    cout << "Enter 3 quiz scores for John: ";
    cin >> quiz1_1 >> quiz2_1 >> quiz3_1;
    cout << "Enter 3 quiz scores for Mary: ";
    cin >> quiz1_2 >> quiz2_2 >> quiz3_2;
    cout << "Enter 3 quiz scores for Matthew: ";
    cin >> quiz1_3 >> quiz2_3 >> quiz3_3;
    cout << endl;
    cout << "Name        Quiz1     Quiz2     Quiz3" << endl;
    cout << "--------    ------    ------    ------" << endl;
    cout << "John" << setw(12) << quiz1_1 << setw(10) << quiz2_1 << setw(10) << quiz3_1 << endl;
    cout << "Mary" << setw(12) << quiz1_2 << setw(10) << quiz2_2 << setw(10) << quiz3_2 << endl;
    cout << "Matthew" << setw(9) << quiz1_3 << setw(10) << quiz2_3 << setw(10) << quiz3_3 << endl;
    cout << endl;
    //Average
    average1 = static_cast<double>(quiz1_1 + quiz1_2 + quiz1_3) / 3;
    average2 = static_cast<double>(quiz2_1 + quiz2_2 + quiz2_3) / 3;
    average3 = static_cast<double>(quiz3_1 + quiz3_2 + quiz3_3) / 3;
    cout << "Average" << setw(9) << average1 << setw(10) << average2 << setw(10) << average3;
    
    return 0;
}

