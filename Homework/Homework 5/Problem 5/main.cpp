#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	char answer;
	int gas;
	double liters = 0.264179;
	answer = 'y';
	while (answer == 'y')
	{
		cout << "Enter liters of gas consumed by car: ";
		cin >> gas;
		double miles = static_cast<double>(gas) * liters;
		cout << "MPG: " << setprecision(4) << miles << endl;
		cout << "Continue? y/n: ";
		cin >> answer;
	}

	return 0;
}