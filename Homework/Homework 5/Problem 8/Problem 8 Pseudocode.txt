char answer; //Loop variable
ifstream infile; //Infile declaration
//Variables
int gas1, gas2;
double liters = 0.264179;
answer = 'y';

infile.open("data.dat");	
cout << "Enter liters of gas consumed by first car: ";
infile >> gas1;
double miles = gas1 * liters;
cout << "MPG: " << setprecision(4) << miles << endl;
infile.close();
cout << "Enter liters of gas consumed by second car: ";
infile >> gas2;
double miles = gas2 * liters;

//Announce which car has better feul efficiency.
if gas1 > gas2
		cout << "First car has better mileage." << endl;
else
		cout << "Second car has better mileage>" << endl;

