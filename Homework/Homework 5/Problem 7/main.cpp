#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int main()
{
	char answer;
	ifstream infile;
	int gas;
	double liters = 0.264179;
	answer = 'y';
	while (answer == 'y')
	{
		infile.open("data.dat");
		cout << "Enter liters of gas consumed by car: ";
		infile >> gas;
		double miles = static_cast<double>(gas) * liters;
		cout << "MPG: " << setprecision(4) << miles << endl;
		infile.close();
		cout << "Continue? y/n: ";
		cin >> answer;
	}

	return 0;
}