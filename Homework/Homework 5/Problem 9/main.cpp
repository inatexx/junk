#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int main()
{
	char answer;
	ifstream infile;
	int gas1, gas2;
	double liters = 0.264179;
	
	//1st
	infile.open("data.dat");
	cout << "Enter liters of gas consumed by first car: ";
	infile >> gas1;
	double miles1 = static_cast<double>(gas1) * liters;
	cout << "MPG: " << setprecision(4) << miles1 << endl;

	//2nd
	cout << "Enter liters of gas consumed by second car: ";
	infile >> gas2;
	double miles2 = static_cast<double>(gas2) * liters;
	cout << "MPG: " << setprecision(4) << miles2 << endl;

	//Efficiency
	if (miles1 > miles2)
		cout << "First car has better mileage." << endl;
	else
		cout << "Second car has better mileage." << endl;

	infile.close();

	return 0;
}