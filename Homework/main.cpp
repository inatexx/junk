#include <iostream>
using namespace std;
int main()
{       
    int temp;
    int a = 5;
    int b = 10;
    cout << "a: " << a << " " << "b: " << b << endl;
    temp = a;
    a = b;
    b = a;
    b = temp;
    cout << "a: " << a << " " << "b: " << b << endl;
    
    return 0;
}